﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectUni.Models
{
    public class Job
    {
        [Key]
        public int JobId { get; set; }

        [Display(Name = "Job Title"), Required]
        [StringLength(50, ErrorMessage = "Characters should be under the range of 50")]
        [RegularExpression(@"^[a-zA-Z]+(\s{0,1}[a-zA-Z-, ])*$", ErrorMessage = "Use alphabets only please")]
        public string JobTitle { get; set; }

        [Display(Name = "Job Designation")]
        public string JobDesignation { get; set; }

        [Display(Name = "Job Description"), Required]
        [AllowHtml]
        public string JobDescription { get; set; }

        [Display(Name = "Skills"), Required]
        public string Skills { get; set; }

        [Display(Name = "Job Type"), Required]
        public string JobType { get; set; }

        public int CityId { get; set; }
        public virtual Cities Cities { get; set; }

        [Display(Name = "Job Shift"), Required]
        public string JobShift { get; set; }

        public int IndustryId { get; set; }
        public virtual Industries Industries { get; set; }

        [Display(Name = "Career Level"), Required]
        public string CareerLevel { get; set; }

        [Display(Name = "Experience"), Required]
        public string Experience { get; set; }

        [Display(Name = "Minimum Salary"), Required]
        public int MinSalary { get; set; }

        [Display(Name = "Miximum Salary"), Required]
        public int MixSalary { get; set; }

        [Display(Name = "Gender"), Required]
        public string Gender { get; set; }

        [Display(Name = "Positions Available"), Required]
        [Range(1, 30, ErrorMessage = "Please Enter value between 1-30")]
        public int Positions { get; set; }

        [Display(Name = "Job Posted on")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string JobDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [Display(Name = "Apply Before"), Required]
        public DateTime ApplyBefore { get; set; }

        [Display(Name = "Minimum Age"), Required]
        [Range(18, 50, ErrorMessage = "Please Enter value between 18-50")]
        public int MinAge { get; set; }

        [Display(Name = "Maximum Age"), Required]
        [Range(18, 50, ErrorMessage = "Please Enter value between 18-50")]
        public int MaxAge { get; set; }

        //Educational info
        [Display(Name = "Qualification Level")]
        public int? LevelId { get; set; }
        public virtual DegreeLevel degreelevel { get; set; }

        [Display(Name = "Major/Specilization")]
        public int? DegreeId { get; set; }
        public virtual DegreeArea degreearea { get; set; }

        public string Status { get; set; }

        public string Company { get; set; }

        [Display(Name = "Apply By")]
        public int? ApplyId { get; set; }
        public virtual JobApplication JobApplication { get; set; }
    }

    public class JobApplication
    {
        [Key]
        public int ApplyId { get; set; }

        [Display(Name = "Email")]
        [EmailAddress, StringLength(50)]
        public string Applyby { get; set; }

        public int JobId { get; set; }

    }

}