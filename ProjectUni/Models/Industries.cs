﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectUni.Models
{
    public class Industries
    {
        [Key]
        public int IndustryId { get; set; }
        public string IndustryName { get; set; }
        

        public virtual ICollection<CityIndustries> CityIndustries { get; set; }
    }
}