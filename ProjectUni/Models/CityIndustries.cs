﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectUni.Models
{
    public class CityIndustries
    {
        [Key]
        [Column(Order = 1)]
        public int CityId { get; set; }
        [Key]
        [Column(Order = 2)]
        public int IndustryId { get; set; }

        public virtual Cities cities { get; set; }
        public virtual Industries industries { get; set; }
    }
}