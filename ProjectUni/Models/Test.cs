﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectUni.Models
{
    public class Test
    {
        [Key]
        [Display(Name = "Question Id")]
        public int QuesId { get; set; }

        [Display(Name = "Question Text"), Required]
        public string QuesText { get; set; }

        [Display(Name = "Choice 1")]
        public string Choice1 { get; set; }

        [Display(Name = "Choice 2")]
        public string Choice2 { get; set; }

        [Display(Name = "Choice 3")]
        public string Choice3 { get; set; }

        [Display(Name = "Choice 4")]
        public string Choice4 { get; set; }

        [Display(Name = "Correct Choice")]
        public Int16 CorrectChoice { get; set; }

        [Display(Name = "Topic Category")]
        public int T_ID { get; set; }
        public virtual Topics topics { get; set; }
    }

    public class Result
    {
        [Key]
        [Display(Name = "Question Id")]
        public int ResultId { get; set; }

        public string UserEmail { get; set; }

        [Display(Name = "Topic Category")]
        public int T_ID { get; set; }
        public virtual Topics topics { get; set; }

        public int TotalQues { get; set; }

        public int CorrectAns { get; set; }

        public int WrontAns { get; set; }

        public int TotalMarks { get; set; }

        public int ObtainedMarks { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Date { get; set; }

        public bool Status { get; set; }
    }

}