﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectUni.Models
{
    public class Videos
    {
        [Key]
        public int V_ID { get; set; }

        [Display(Name = "Video Name"), Required]
        //[RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Use Alphabets and Numbers Only")]
        public string Name { get; set; }

        [Display(Name = "Video URL"), Required]
        [Url(ErrorMessage = "Please enter a valid url")]
        public string URL { get; set; }

        [Display(Name = "Topic Category")]
        public int T_ID { get; set; }
        public virtual Topics topics { get; set; }
    }
}