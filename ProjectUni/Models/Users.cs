﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectUni.Models
{
    public class Users
    {
        [Key]
        [Display(Name = "User Id")]
        public int UserId { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name"), Required]
        public string LastName { get; set; }

        [Display(Name = "Email"), Required]
        [EmailAddress, StringLength(50)]
        public string Email { get; set; }

        [Display(Name = "Gender"), Required]
        public string Gender { get; set; }

        [Display(Name = "User Image")]
        public string Image { get; set; }

        [Display(Name = "Date of Birth"), Required]
        [DateOfBirth(MinAge = 18, MaxAge = 100)]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateofBirth { get; set; }

        [Required(ErrorMessage = "Phone number is Required")]
        [RegularExpression(@"^\+92[0-9]{3}-[0-9]{7}$", ErrorMessage = "Format for Contact is +92xxx-xxxxxxx")]
        [StringLength(14, ErrorMessage = "Phone number should be less than {1} characters")]
        [Display(Name = "Phone Number")]
        public string Contact { get; set; }

        [Required(ErrorMessage = "ID card number is Required")]
        [RegularExpression(@"^\d{5}-\d{7}-\d{1}", ErrorMessage = "Format for CNIC is xxxxx-xxxxxxx-x")]
        [StringLength(15, ErrorMessage = "Id card number should be 15 characters")]
        [Display(Name = "CNIC")]
        public string CNIC { get; set; }

        [Display(Name = "Address"), Required]
        [StringLength(100, ErrorMessage = "Address should be between 15 to 100 characters", MinimumLength = 15)]
        public string Address { get; set; }

        public int CityId { get; set; }

        public virtual Cities Cities { get; set; }

        //Educational info
        public int? LevelId { get; set; }

        public virtual DegreeLevel degreelevel { get; set; }

        public int? DegreeId { get; set; }

        public virtual DegreeArea degreearea { get; set; }

        public string Institute { get; set; }

        [Display(Name = "Date of Completion")]
        //[Range(14, 50, ErrorMessage = "Please Enter value between 1-30")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public Nullable<DateTime> DateofCompletion { get; set; }

        public string Grade { get; set; }

        //Other Skills
        public string Skills { get; set; }

        //Past Experience
        public string OrganizationName { get; set; }
        public string Duration { get; set; }
        public string Responsibility { get; set; }

    }
}