﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectUni.Models
{
    public class InstituteCourse
    {
        [Key]
        [Column(Order = 1)]
        public int InstituteId { get; set; }
        [Key]
        [Column(Order = 2)]
        public int CourseId { get; set; }

        public string Fee { get; set; }

        public string Duration { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Session Start at")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime StartDate { get; set; }

        public virtual Institute Institute { get; set; }
        public virtual Course Course { get; set; }
    }
}