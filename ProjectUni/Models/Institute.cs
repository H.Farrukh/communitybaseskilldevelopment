﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectUni.Models
{
    public class Institute
    {
        [Key]
        public int InstituteId { get; set; }

        public string InstituteName { get; set; }

        public string Description { get; set; }

        [Url(ErrorMessage = "Please enter a valid url")]
        [DisplayName("Website")]
        public string Website { get; set; }

        [Display(Name = "Company Logo")]
        public string Image { get; set; }

        [Required(ErrorMessage = "Phone number is Required")]
        [RegularExpression(@"^0[0-9]{2,3}-[0-9]{7}$", ErrorMessage = "Format for Contact is 0xxx-xxxxxxx")]
        [StringLength(12, ErrorMessage = "Phone number should be 11 or 12 characters depending on code")]
        [Display(Name = "Phone Number")]
        public string Contact { get; set; }

        public string Address { get; set; }

        [Display(Name = "Operating Since"), Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string OperatingSince { get; set; }

        public int CityId { get; set; }
        public virtual Cities Cities { get; set; }

        //Contact Person Info
        [Display(Name = "Email"), Required]
        [EmailAddress, StringLength(50)]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Contact Person")]
        public string PersonName { get; set; }

        [Required]
        public string Designation { get; set; }

        [Required(ErrorMessage = "Mobile number is Required")]
        [RegularExpression(@"^\+92[0-9]{3}-[0-9]{7}$", ErrorMessage = "Format for Contact is +92xxx-xxxxxxx")]
        [StringLength(14, ErrorMessage = "Phone number should be 14 characters")]
        [Display(Name = "Mobile Number")]
        public string Mobile { get; set; }

        public string AffiliatedBy { get; set; }

        public bool Status { get; set; }

        public virtual ICollection<InstituteCourse> InstituteCourse { get; set; }


    }

    public class Course
    {
        [Key]
        public int CourseId { get; set; }

        public string Description { get; set; }
     
        public string CourseName { get; set; } 
        
        public bool Status { get; set; }

        public virtual ICollection<InstituteCourse> InstituteCourse { get; set; }

    }

}