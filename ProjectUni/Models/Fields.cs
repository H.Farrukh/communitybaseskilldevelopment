﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectUni.Models
{
    public class Fields
    {
        [Key]
        public int FieldId { get; set; }
        public string FieldName { get; set; }

    }
}