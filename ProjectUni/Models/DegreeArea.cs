﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectUni.Models
{
    public class DegreeArea
    {
        [Key]
        public int DegreeId { get; set; }

        [Display(Name = "Degree Area")]
        public string AreaName { get; set; }

        public int? LevelId { get; set; }

        public virtual DegreeLevel degreelevel { get; set; }
    }
}