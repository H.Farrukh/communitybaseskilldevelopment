﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectUni.Models
{
    public class Notes
    {
        [Key]
        public int N_ID { get; set; }

        [Display(Name = "Notes"), Required]
        [AllowHtml]
        public string TNotes { get; set; }

        [Display(Name = "Topic Category")]
        public int T_ID { get; set; }
        public virtual Topics topics { get; set; }
    }
}