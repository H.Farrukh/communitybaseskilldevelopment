﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProjectUni.Models
{
    public class PContext : DbContext
    {
        public DbSet<Users> users { get; set; }
        public DbSet<Registration> Registration { get; set; }
        public DbSet<Cities> cities { get; set; }
        public DbSet<Companies> Companies { get; set; }
        public DbSet<Industries> industries { get; set; }
        public DbSet<Institute> institute { get; set; }
        public DbSet<Fields> fields { get; set; }
        public DbSet<CityIndustries> cityIndustries { get; set; }
        public DbSet<InstituteCourse> InstituteCourse { get; set; }
        public DbSet<DegreeArea> DegreeAreas { get; set; }
        public DbSet<DegreeLevel> DegreeLevels { get; set; }
        public DbSet<Job> Job { get; set; }
        public DbSet<JobApplication> JobApplication { get; set; }
        public DbSet<Topics> Topics { get; set; }
        public DbSet<Notes> Notes { get; set; }
        public DbSet<Course> Course { get; set; }
        public DbSet<Videos> Videos { get; set; }
        public DbSet<Test> Test { get; set; }
        public DbSet<Result> Result { get; set; }

    }
}