﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectUni.Models
{
    public class Topics
    {
        [Key]
        public int T_ID { get; set; }

        [Display(Name = "Topic Name"), Required]
        public string TName { get; set; }

        [Display(Name = "Field Category")]
        public int FieldId { get; set; }
        public virtual Fields Fields { get; set; }
    }
}