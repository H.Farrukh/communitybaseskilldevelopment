﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectUni.Models
{
    public class DegreeLevel
    {
        [Key]
        public int LevelId { get; set; }

        [Display(Name = "Degree Level")]
        public string LevelName { get; set; }
    }
}