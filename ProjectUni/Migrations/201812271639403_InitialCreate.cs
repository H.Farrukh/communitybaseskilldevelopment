namespace ProjectUni.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        CityId = c.Int(nullable: false, identity: true),
                        CityName = c.String(),
                    })
                .PrimaryKey(t => t.CityId);
            
            CreateTable(
                "dbo.CityIndustries",
                c => new
                    {
                        CityId = c.Int(nullable: false),
                        IndustryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CityId, t.IndustryId })
                .ForeignKey("dbo.Cities", t => t.CityId, cascadeDelete: true)
                .ForeignKey("dbo.Industries", t => t.IndustryId, cascadeDelete: true)
                .Index(t => t.CityId)
                .Index(t => t.IndustryId);
            
            CreateTable(
                "dbo.Industries",
                c => new
                    {
                        IndustryId = c.Int(nullable: false, identity: true),
                        IndustryName = c.String(),
                    })
                .PrimaryKey(t => t.IndustryId);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        CompanyId = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(),
                        Description = c.String(),
                        Website = c.String(),
                        Image = c.String(),
                        Contact = c.String(nullable: false, maxLength: 12),
                        Address = c.String(),
                        NoOfEmployee = c.Int(nullable: false),
                        OperatingSince = c.String(nullable: false),
                        IndustryId = c.Int(nullable: false),
                        CityId = c.Int(nullable: false),
                        Email = c.String(nullable: false, maxLength: 50),
                        PersonName = c.String(nullable: false),
                        Designation = c.String(nullable: false),
                        Mobile = c.String(nullable: false, maxLength: 14),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CompanyId)
                .ForeignKey("dbo.Cities", t => t.CityId, cascadeDelete: true)
                .ForeignKey("dbo.Industries", t => t.IndustryId, cascadeDelete: true)
                .Index(t => t.IndustryId)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        CourseId = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        CourseName = c.String(),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CourseId);
            
            CreateTable(
                "dbo.InstituteCourses",
                c => new
                    {
                        InstituteId = c.Int(nullable: false),
                        CourseId = c.Int(nullable: false),
                        Fee = c.String(),
                        Duration = c.String(),
                        StartDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.InstituteId, t.CourseId })
                .ForeignKey("dbo.Courses", t => t.CourseId, cascadeDelete: true)
                .ForeignKey("dbo.Institutes", t => t.InstituteId, cascadeDelete: true)
                .Index(t => t.InstituteId)
                .Index(t => t.CourseId);
            
            CreateTable(
                "dbo.Institutes",
                c => new
                    {
                        InstituteId = c.Int(nullable: false, identity: true),
                        InstituteName = c.String(),
                        Description = c.String(),
                        Website = c.String(),
                        Image = c.String(),
                        Contact = c.String(nullable: false, maxLength: 12),
                        Address = c.String(),
                        OperatingSince = c.String(nullable: false),
                        CityId = c.Int(nullable: false),
                        Email = c.String(nullable: false, maxLength: 50),
                        PersonName = c.String(nullable: false),
                        Designation = c.String(nullable: false),
                        Mobile = c.String(nullable: false, maxLength: 14),
                        AffiliatedBy = c.String(),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.InstituteId)
                .ForeignKey("dbo.Cities", t => t.CityId, cascadeDelete: true)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.DegreeAreas",
                c => new
                    {
                        DegreeId = c.Int(nullable: false, identity: true),
                        AreaName = c.String(),
                        LevelId = c.Int(),
                    })
                .PrimaryKey(t => t.DegreeId)
                .ForeignKey("dbo.DegreeLevels", t => t.LevelId)
                .Index(t => t.LevelId);
            
            CreateTable(
                "dbo.DegreeLevels",
                c => new
                    {
                        LevelId = c.Int(nullable: false, identity: true),
                        LevelName = c.String(),
                    })
                .PrimaryKey(t => t.LevelId);
            
            CreateTable(
                "dbo.Fields",
                c => new
                    {
                        FieldId = c.Int(nullable: false, identity: true),
                        FieldName = c.String(),
                    })
                .PrimaryKey(t => t.FieldId);
            
            CreateTable(
                "dbo.Jobs",
                c => new
                    {
                        JobId = c.Int(nullable: false, identity: true),
                        JobTitle = c.String(nullable: false, maxLength: 50),
                        JobDesignation = c.String(),
                        JobDescription = c.String(nullable: false),
                        Skills = c.String(nullable: false),
                        JobType = c.String(nullable: false),
                        CityId = c.Int(nullable: false),
                        JobShift = c.String(nullable: false),
                        IndustryId = c.Int(nullable: false),
                        CareerLevel = c.String(nullable: false),
                        Experience = c.String(nullable: false),
                        MinSalary = c.Int(nullable: false),
                        MixSalary = c.Int(nullable: false),
                        Gender = c.String(nullable: false),
                        Positions = c.Int(nullable: false),
                        JobDate = c.String(),
                        ApplyBefore = c.DateTime(nullable: false),
                        MinAge = c.Int(nullable: false),
                        MaxAge = c.Int(nullable: false),
                        LevelId = c.Int(),
                        DegreeId = c.Int(),
                        Status = c.String(),
                        Company = c.String(),
                        ApplyId = c.Int(),
                    })
                .PrimaryKey(t => t.JobId)
                .ForeignKey("dbo.Cities", t => t.CityId, cascadeDelete: true)
                .ForeignKey("dbo.DegreeAreas", t => t.DegreeId)
                .ForeignKey("dbo.DegreeLevels", t => t.LevelId)
                .ForeignKey("dbo.Industries", t => t.IndustryId, cascadeDelete: true)
                .ForeignKey("dbo.JobApplications", t => t.ApplyId)
                .Index(t => t.CityId)
                .Index(t => t.IndustryId)
                .Index(t => t.LevelId)
                .Index(t => t.DegreeId)
                .Index(t => t.ApplyId);
            
            CreateTable(
                "dbo.JobApplications",
                c => new
                    {
                        ApplyId = c.Int(nullable: false, identity: true),
                        Applyby = c.String(maxLength: 50),
                        JobId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ApplyId);
            
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        N_ID = c.Int(nullable: false, identity: true),
                        TNotes = c.String(nullable: false),
                        T_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.N_ID)
                .ForeignKey("dbo.Topics", t => t.T_ID, cascadeDelete: true)
                .Index(t => t.T_ID);
            
            CreateTable(
                "dbo.Topics",
                c => new
                    {
                        T_ID = c.Int(nullable: false, identity: true),
                        TName = c.String(nullable: false),
                        FieldId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.T_ID)
                .ForeignKey("dbo.Fields", t => t.FieldId, cascadeDelete: true)
                .Index(t => t.FieldId);
            
            CreateTable(
                "dbo.Registrations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(nullable: false, maxLength: 50),
                        Password = c.String(nullable: false, maxLength: 100),
                        ConfirmPassword = c.String(),
                        UserCategory = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Results",
                c => new
                    {
                        ResultId = c.Int(nullable: false, identity: true),
                        UserEmail = c.String(),
                        T_ID = c.Int(nullable: false),
                        TotalQues = c.Int(nullable: false),
                        CorrectAns = c.Int(nullable: false),
                        WrontAns = c.Int(nullable: false),
                        TotalMarks = c.Int(nullable: false),
                        ObtainedMarks = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ResultId)
                .ForeignKey("dbo.Topics", t => t.T_ID, cascadeDelete: true)
                .Index(t => t.T_ID);
            
            CreateTable(
                "dbo.Tests",
                c => new
                    {
                        QuesId = c.Int(nullable: false, identity: true),
                        QuesText = c.String(nullable: false),
                        Choice1 = c.String(),
                        Choice2 = c.String(),
                        Choice3 = c.String(),
                        Choice4 = c.String(),
                        CorrectChoice = c.Short(nullable: false),
                        T_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QuesId)
                .ForeignKey("dbo.Topics", t => t.T_ID, cascadeDelete: true)
                .Index(t => t.T_ID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Email = c.String(nullable: false, maxLength: 50),
                        Gender = c.String(nullable: false),
                        Image = c.String(),
                        DateofBirth = c.DateTime(nullable: false),
                        Contact = c.String(nullable: false, maxLength: 14),
                        CNIC = c.String(nullable: false, maxLength: 15),
                        Address = c.String(nullable: false, maxLength: 100),
                        CityId = c.Int(nullable: false),
                        LevelId = c.Int(),
                        DegreeId = c.Int(),
                        Institute = c.String(),
                        DateofCompletion = c.DateTime(),
                        Grade = c.String(),
                        Skills = c.String(),
                        OrganizationName = c.String(),
                        Duration = c.String(),
                        Responsibility = c.String(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Cities", t => t.CityId, cascadeDelete: true)
                .ForeignKey("dbo.DegreeAreas", t => t.DegreeId)
                .ForeignKey("dbo.DegreeLevels", t => t.LevelId)
                .Index(t => t.CityId)
                .Index(t => t.LevelId)
                .Index(t => t.DegreeId);
            
            CreateTable(
                "dbo.Videos",
                c => new
                    {
                        V_ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        URL = c.String(nullable: false),
                        T_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.V_ID)
                .ForeignKey("dbo.Topics", t => t.T_ID, cascadeDelete: true)
                .Index(t => t.T_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Videos", "T_ID", "dbo.Topics");
            DropForeignKey("dbo.Users", "LevelId", "dbo.DegreeLevels");
            DropForeignKey("dbo.Users", "DegreeId", "dbo.DegreeAreas");
            DropForeignKey("dbo.Users", "CityId", "dbo.Cities");
            DropForeignKey("dbo.Tests", "T_ID", "dbo.Topics");
            DropForeignKey("dbo.Results", "T_ID", "dbo.Topics");
            DropForeignKey("dbo.Notes", "T_ID", "dbo.Topics");
            DropForeignKey("dbo.Topics", "FieldId", "dbo.Fields");
            DropForeignKey("dbo.Jobs", "ApplyId", "dbo.JobApplications");
            DropForeignKey("dbo.Jobs", "IndustryId", "dbo.Industries");
            DropForeignKey("dbo.Jobs", "LevelId", "dbo.DegreeLevels");
            DropForeignKey("dbo.Jobs", "DegreeId", "dbo.DegreeAreas");
            DropForeignKey("dbo.Jobs", "CityId", "dbo.Cities");
            DropForeignKey("dbo.DegreeAreas", "LevelId", "dbo.DegreeLevels");
            DropForeignKey("dbo.InstituteCourses", "InstituteId", "dbo.Institutes");
            DropForeignKey("dbo.Institutes", "CityId", "dbo.Cities");
            DropForeignKey("dbo.InstituteCourses", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.Companies", "IndustryId", "dbo.Industries");
            DropForeignKey("dbo.Companies", "CityId", "dbo.Cities");
            DropForeignKey("dbo.CityIndustries", "IndustryId", "dbo.Industries");
            DropForeignKey("dbo.CityIndustries", "CityId", "dbo.Cities");
            DropIndex("dbo.Videos", new[] { "T_ID" });
            DropIndex("dbo.Users", new[] { "DegreeId" });
            DropIndex("dbo.Users", new[] { "LevelId" });
            DropIndex("dbo.Users", new[] { "CityId" });
            DropIndex("dbo.Tests", new[] { "T_ID" });
            DropIndex("dbo.Results", new[] { "T_ID" });
            DropIndex("dbo.Topics", new[] { "FieldId" });
            DropIndex("dbo.Notes", new[] { "T_ID" });
            DropIndex("dbo.Jobs", new[] { "ApplyId" });
            DropIndex("dbo.Jobs", new[] { "DegreeId" });
            DropIndex("dbo.Jobs", new[] { "LevelId" });
            DropIndex("dbo.Jobs", new[] { "IndustryId" });
            DropIndex("dbo.Jobs", new[] { "CityId" });
            DropIndex("dbo.DegreeAreas", new[] { "LevelId" });
            DropIndex("dbo.Institutes", new[] { "CityId" });
            DropIndex("dbo.InstituteCourses", new[] { "CourseId" });
            DropIndex("dbo.InstituteCourses", new[] { "InstituteId" });
            DropIndex("dbo.Companies", new[] { "CityId" });
            DropIndex("dbo.Companies", new[] { "IndustryId" });
            DropIndex("dbo.CityIndustries", new[] { "IndustryId" });
            DropIndex("dbo.CityIndustries", new[] { "CityId" });
            DropTable("dbo.Videos");
            DropTable("dbo.Users");
            DropTable("dbo.Tests");
            DropTable("dbo.Results");
            DropTable("dbo.Registrations");
            DropTable("dbo.Topics");
            DropTable("dbo.Notes");
            DropTable("dbo.JobApplications");
            DropTable("dbo.Jobs");
            DropTable("dbo.Fields");
            DropTable("dbo.DegreeLevels");
            DropTable("dbo.DegreeAreas");
            DropTable("dbo.Institutes");
            DropTable("dbo.InstituteCourses");
            DropTable("dbo.Courses");
            DropTable("dbo.Companies");
            DropTable("dbo.Industries");
            DropTable("dbo.CityIndustries");
            DropTable("dbo.Cities");
        }
    }
}
