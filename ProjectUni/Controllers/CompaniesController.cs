﻿using ProjectUni.Models;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace ProjectUni.Controllers
{
    [AuthorizeUserAccessLevel(UserRole = "Company")]
    public class CompaniesController : Controller
    {
        private PContext db = new PContext();

        // GET: Companies
        public ActionResult Index()
        {
            var companies = db.Companies.Include(c => c.Cities).Include(c => c.industries);
            return View(companies.ToList());
        }

        // GET: Companies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (id != (int)Session["CompanyID"])
            {
                return HttpNotFound();
            }
            Companies companies = db.Companies.Find(id);
            if (companies == null)
            {
                return HttpNotFound();
            }
            return View(companies);
        }

        // GET: Companies/Create
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName");
            ViewBag.IndustryId = new SelectList(db.industries, "IndustryId", "IndustryName");
            return View();
        }

        // POST: Companies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CompanyName,Description,Website,Image,Contact,Address,NoOfEmployee,OperatingSince,IndustryId,CityId,Email,PersonName,Designation,Mobile")] Companies companies, HttpPostedFileBase Image)
        {
            if (ModelState.IsValid)
            {
                if (Image != null)
                {

                    var filename = Path.GetFileName(Image.FileName);
                    var extension = Path.GetExtension(filename).ToLower();
                    if (extension == ".jpg" || extension == ".png" || extension == ".jpeg" || extension == ".gif" || extension == ".bmp" || extension == ".jpeg" || extension == ".gif" || extension == ".bmp")
                    {
                        var path = HostingEnvironment.MapPath(Path.Combine("~/ProfileImages/", filename));
                        Image.SaveAs(path);
                        companies.Image = "~/ProfileImages/" + filename;
                        
                        db.Companies.Add(companies);
                        db.SaveChanges();
                        return RedirectToAction("Index");

                    }
                    if (extension != ".jpg" || extension != ".png" || extension != ".jpeg" || extension != ".gif" || extension != ".bmp" || extension != ".jpeg" || extension != ".gif" || extension != ".bmp")
                    {
                        ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", companies.CityId);
                        ViewBag.IndustryId = new SelectList(db.industries, "IndustryId", "IndustryName", companies.IndustryId);
                        return View(companies);
                    }
                }
                
            }

            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", companies.CityId);
            ViewBag.IndustryId = new SelectList(db.industries, "IndustryId", "IndustryName", companies.IndustryId);
            return View(companies);
        }

        // GET: Companies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Companies companies = db.Companies.Find(id);
            if (companies == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", companies.CityId);
            ViewBag.IndustryId = new SelectList(db.industries, "IndustryId", "IndustryName", companies.IndustryId);
            return View(companies);
        }

        // POST: Companies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Companies companies, HttpPostedFileBase Image)
        {
            if (Image != null)
            {
                var filename = Path.GetFileName(Image.FileName);
                var extension = Path.GetExtension(filename).ToLower();
                if (extension == ".jpg" || extension == ".png" || extension == ".jpeg" || extension == ".gif" || extension == ".bmp")
                {
                    var path = HostingEnvironment.MapPath(Path.Combine("~/Images/", filename));
                    Image.SaveAs(path);
                    companies.Image = "~/Images/" + filename;
                }
                else
                if (extension != ".jpg" || extension != ".png" || extension != ".jpeg" || extension != ".gif" || extension != ".bmp")
                {
                    ViewBag.ImageError = "Choose the image file.";
                    ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", companies.CityId);
                    ViewBag.IndustryId = new SelectList(db.industries, "IndustryId", "IndustryName", companies.IndustryId);
                    return View(companies);
                }
            }
            if (ModelState.IsValid)
            {
                db.Entry(companies).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", new { id = companies.CompanyId });
            }
            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", companies.CityId);
            ViewBag.IndustryId = new SelectList(db.industries, "IndustryId", "IndustryName", companies.IndustryId);
            return View(companies);
        }

        // GET: Companies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Companies companies = db.Companies.Find(id);
            if (companies == null)
            {
                return HttpNotFound();
            }
            return View(companies);
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Companies companies = db.Companies.Find(id);
            db.Companies.Remove(companies);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult JobIndex()
        {
            var job = db.Job.Include(j => j.Cities).Include(j => j.degreearea).Include(j => j.degreelevel).Include(j => j.Industries);
            return View(job.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
