﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using ProjectUni.Models;

namespace ProjectUni.Controllers
{
    [AuthorizeUserAccessLevel(UserRole = "Institute")]
    public class InstitutesController : Controller
    {
        private PContext db = new PContext();

        // GET: Institutes
        public ActionResult Index()
        {
            var institute = db.institute.Include(i => i.Cities);
            return View(institute.ToList());
        }

        public ActionResult Courses()
        {
            var Courses = db.Course;
            return View(Courses.ToList());
        }

        // GET: Institutes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (id != (int)Session["InstituteID"])
            {
                return HttpNotFound();
            }
            Institute institute = db.institute.Find(id);
            if (institute == null)
            {
                return HttpNotFound();
            }
            return View(institute);
        }

        // GET: Institutes/Create
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName");
            return View();
        }

        // POST: Institutes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InstituteId,InstituteName,Description,Website,Image,Contact,Address,OperatingSince,CityId,Email,PersonName,Designation,Mobile,AffiliatedBy,Status")] Institute institute, HttpPostedFileBase Image)
        {
            if (ModelState.IsValid)
            {
                if (Image != null)
                {

                    var filename = Path.GetFileName(Image.FileName);
                    var extension = Path.GetExtension(filename).ToLower();
                    if (extension == ".jpg" || extension == ".png" || extension == ".jpeg" || extension == ".gif" || extension == ".bmp" || extension == ".jpeg" || extension == ".gif" || extension == ".bmp")
                    {
                        var path = HostingEnvironment.MapPath(Path.Combine("~/ProfileImages/", filename));
                        Image.SaveAs(path);
                        institute.Image = "~/ProfileImages/" + filename;

                        db.institute.Add(institute);
                        db.SaveChanges();
                        return RedirectToAction("Details");

                    }
                    if (extension != ".jpg" || extension != ".png" || extension != ".jpeg" || extension != ".gif" || extension != ".bmp" || extension != ".jpeg" || extension != ".gif" || extension != ".bmp")
                    {
                        ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", institute.CityId);
                        return View(institute);
                    }
                }

            }

            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", institute.CityId);
            return View(institute);
        }

        // GET: Institutes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Institute institute = db.institute.Find(id);
            if (institute == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", institute.CityId);
            return View(institute);
        }

        // POST: Institutes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "InstituteId,InstituteName,Description,Website,Image,Contact,Address,OperatingSince,CityId,Email,PersonName,Designation,Mobile,AffiliatedBy,Status")] Institute institute, HttpPostedFileBase Image)
        {
            if (Image != null)
            {
                var filename = Path.GetFileName(Image.FileName);
                var extension = Path.GetExtension(filename).ToLower();
                if (extension == ".jpg" || extension == ".png" || extension == ".jpeg" || extension == ".gif" || extension == ".bmp")
                {
                    var path = HostingEnvironment.MapPath(Path.Combine("~/Images/", filename));
                    Image.SaveAs(path);
                    institute.Image = "~/Images/" + filename;
                }
                else
                if (extension != ".jpg" || extension != ".png" || extension != ".jpeg" || extension != ".gif" || extension != ".bmp")
                {
                    ViewBag.ImageError = "Choose the image file.";
                    ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", institute.CityId);
                    return View(institute);
                }
            }
            if (ModelState.IsValid)
            {
                db.Entry(institute).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("details", new { id = institute.InstituteId });
            }
            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", institute.CityId);
            return View(institute);
        }

        // GET: Institutes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Institute institute = db.institute.Find(id);
            if (institute == null)
            {
                return HttpNotFound();
            }
            return View(institute);
        }

        // POST: Institutes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Institute institute = db.institute.Find(id);
            db.institute.Remove(institute);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
