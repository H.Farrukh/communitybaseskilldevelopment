﻿using ProjectUni.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectUni.Controllers
{
    [AuthorizeUserAccessLevel(UserRole = "Admin")]
    public class AdminController : Controller
    {
        private PContext db = new PContext();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        // GET: Users
        public ActionResult Users()
        {
            var users = db.users;
            return View(users.ToList());
        }

        public ActionResult Companies()
        {
            var companies = db.Companies;
            return View(companies.ToList());
        }

        public ActionResult Institutes()
        {
            var institutes = db.institute;
            return View(institutes.ToList());
        }

        public ActionResult Jobs()
        {
            var jobs = db.Job;
            return View(jobs.ToList());
        }

    }
}