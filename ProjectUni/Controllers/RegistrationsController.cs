﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectUni.Models;

namespace ProjectUni.Controllers
{ 
    public class RegistrationsController : Controller
    {
        private PContext db = new PContext();

        // GET: Registrations
        public ActionResult Index()
        {
            return View(db.Registration.ToList());
        }

        // GET: Registrations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registration registration = db.Registration.Find(id);
            if (registration == null)
            {
                return HttpNotFound();
            }
            return View(registration);
        }

        // GET: Registrations/Create
        public ActionResult New()
        {
            return View();
        }

        // POST: Registrations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New([Bind(Include = "Id,Email,Password,ConfirmPassword,UserCategory")] Registration registration)
        {
            
            registration.Password = Helper.base64Encode(registration.Password);
            registration.ConfirmPassword = Helper.base64Encode(registration.ConfirmPassword);
            var s = from e in db.Registration where e.Email == registration.Email select e;
            if (s.FirstOrDefault() != null)
            {
                ViewBag.error = "User is already exists.";
                return View(registration);
            }
            if (ModelState.IsValid)
            {
                db.Registration.Add(registration);
                db.SaveChanges();
                return RedirectToAction("Login");
            }

            return View(registration);
        }

        // GET: Registrations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registration registration = db.Registration.Find(id);
            if (registration == null)
            {
                return HttpNotFound();
            }
            return View(registration);
        }

        // POST: Registrations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,Password,ConfirmPassword,UserCategory")] Registration registration)
        {
            if (ModelState.IsValid)
            {
                db.Entry(registration).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(registration);
        }

        // GET: Registrations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registration registration = db.Registration.Find(id);
            if (registration == null)
            {
                return HttpNotFound();
            }
            return View(registration);
        }

        // POST: Registrations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Registration registration = db.Registration.Find(id);
            db.Registration.Remove(registration);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Login()
        {
            Registration registration = db.Registration.FirstOrDefault();
            if (registration == null)
            {
                Registration robj = new Registration();
                robj.Email = "mir.m.rizwan@gmail.com";
                robj.Password = Helper.base64Encode("asd123");
                robj.ConfirmPassword = Helper.base64Encode("asd123");
                robj.UserCategory = "Admin";
                db.Registration.Add(robj);
                db.SaveChanges();
                Session["checking"] += "";
                return View();
            }
            Session["checking"] += "";
            return View();
        }
        [HttpPost]
        public ActionResult Login(string password, string email)
        {

            var db = new Models.PContext();


            password = Helper.base64Encode(password);
            var s = from e in db.Registration where e.Email == email && e.Password == password select e;
            
            try
            {
                if (s.FirstOrDefault() != null)
                {
                    var obj = s.FirstOrDefault();

                    Session["Login"] = obj;
                    Session["Role"] = obj.UserCategory; 
                    Session["Email"] = obj.Email;
                    Session["UserID"] = obj.Id;
                    Session["Authorize"] = obj.UserCategory + obj.Id;
                    
                    //Session["Pic"] = obj.img;

                    var oe = obj.Email;
                    var userobj = db.users.Where(d => d.Email == oe).FirstOrDefault();
                    var comobj = db.Companies.Where(d => d.Email == oe).FirstOrDefault();
                    var insobj = db.institute.Where(d => d.Email == oe).FirstOrDefault();

                    if (obj.UserCategory == "Admin")
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                    else
                    if (obj.UserCategory == "Company")
                    {
                        if (comobj != null)
                        {
                            Session["CompanyID"] = comobj.CompanyId;
                            return RedirectToAction("details", "Companies", new { id = comobj.CompanyId });
                        }
                        return RedirectToAction("Create", "Companies");
                    }
                    else
                    if (obj.UserCategory == "Institute")
                    {
                        if (insobj != null)
                        {
                            Session["InstituteID"] = insobj.InstituteId;
                            return RedirectToAction("details", "Institutes", new { id = insobj.InstituteId });
                        }
                        return RedirectToAction("Create", "Institutes");
                    }
                    else
                    {
                        if(userobj != null)
                        {
                            Session["CandidateID"] = userobj.UserId;
                            return RedirectToAction("ViewInfo_p", "Users", new { id = userobj.UserId });
                        }
                        return RedirectToAction("InsertInfo", "Users");
                    }
                }
                else
                {
                    Session.RemoveAll();
                    Session["checking"] = "Email or Password is not correct.";
                    return RedirectToAction("Login", "Registrations");
                }
            }
            catch (Exception eex)
            {

                return View("Login", "Registrations");
            }
        }
        public ActionResult LogOff()
        {
            Session.Abandon();
            return RedirectToAction("Login", "Registrations");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
