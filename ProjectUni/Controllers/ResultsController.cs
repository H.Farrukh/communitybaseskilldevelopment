﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectUni.Models;

namespace ProjectUni.Controllers
{
    public class ResultsController : Controller
    {
        private PContext db = new PContext();

        // GET: Results
        public ActionResult Index()
        {
            var result = db.Result.Include(r => r.topics);
            return View(result.ToList());
        }

        // GET: Results/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Result result = db.Result.Find(id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // GET: Results/Details/5
        public ActionResult CustomDetails()
        {
            var lastRow = (from r in db.Result orderby r.ResultId descending select r).FirstOrDefault();
            Result result = lastRow;
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        [HttpGet]
        // GET: Results/Create
        public ActionResult Create(string UserEmail,int T_ID,float TotalQues,float CorrectAns)
        {
            
            bool stat = false;
            var WrontAns = TotalQues - CorrectAns;
            //if (WrontAns == 0)
            //    WrontAns++;
            float temp = (CorrectAns / TotalQues) * 100;
            if (temp > 60 ) {

                stat = true;
            }
                var result = new Result() {
                    UserEmail = UserEmail,
                    T_ID = T_ID,
                    TotalQues = (int)TotalQues,
                    CorrectAns = (int)CorrectAns,
                    WrontAns = (int)WrontAns,
                    TotalMarks = (int)(TotalQues * 5),
                    ObtainedMarks = (int)(CorrectAns * 5),
                    Date = DateTime.Now,
                Status = stat,
            };
            db.Result.Add(result);
            db.SaveChanges();
            return RedirectToAction("CustomDetails");
        }

       

        // GET: Results/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Result result = db.Result.Find(id);
            if (result == null)
            {
                return HttpNotFound();
            }
            ViewBag.T_ID = new SelectList(db.Topics, "T_ID", "TName", result.T_ID);
            return View(result);
        }

        // POST: Results/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ResultId,UserEmail,T_ID,TotalQues,CorrectAns,WrontAns,TotalMarks,ObtainedMarks,Status")] Result result)
        {
            if (ModelState.IsValid)
            {
                db.Entry(result).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.T_ID = new SelectList(db.Topics, "T_ID", "TName", result.T_ID);
            return View(result);
        }

        // GET: Results/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Result result = db.Result.Find(id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Results/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Result result = db.Result.Find(id);
            db.Result.Remove(result);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
