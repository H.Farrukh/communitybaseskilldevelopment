﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectUni.Models;

namespace ProjectUni.Controllers
{
    public class InstituteCoursesController : Controller
    {
        private PContext db = new PContext();

        // GET: InstituteCourses
        public ActionResult Index()
        {
            var InstituteCourses = db.InstituteCourse.Include(c => c.Course).Include(c => c.Institute);
            return View(InstituteCourses.ToList());
        }

        public ActionResult InstituteAssignedCourses()
        {
            var id = (int)Session["InstituteID"];
            var InstituteCourses = db.InstituteCourse.Include(c => c.Course).Include(c => c.Institute).Where(i => i.InstituteId == id);
            return View(InstituteCourses.ToList());
        }

        // GET: InstituteCoursess/Create
        //public ActionResult CourseAssignInstitutes()
        //{
        //    ViewBag.CourseId = new SelectList(db.Course, "CourseId", "CourseName");
        //    var Institute = db.institute.ToList();
        //    return View(Institute);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult CourseAssignInstitutes(int CourseId, int[] InstituteId)
        //{
        //    List<InstituteCourse> InstituteCourses = db.InstituteCourse.ToList();
        //    foreach (var Instituteids in InstituteId)
        //    {
        //        foreach (var ci in InstituteCourses)
        //        {
        //            if (ci.CourseId == CourseId && ci.InstituteId == Instituteids)
        //            {
        //                ViewBag.Error = "This relation is already exist.";
        //                ViewBag.CourseId = new SelectList(db.Course, "CourseId", "CourseName");
        //                var Institute = db.institute.ToList();
        //                return View(Institute);
        //            }
        //        }
        //    }
        //    foreach (var Instituteids in InstituteId)
        //    {
        //        InstituteCourse CourseInstitute = new InstituteCourse();
        //        CourseInstitute.CourseId = CourseId;
        //        CourseInstitute.InstituteId = Instituteids;
        //        db.InstituteCourse.Add(CourseInstitute);
        //        db.SaveChanges();
        //    }
        //    return RedirectToAction("index");
        //}

        public ActionResult InstituteAssignCourses()
        {
            var Course = db.Course.ToList();
            return View(Course);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InstituteAssignCourses(int[] CourseId, int InstituteId, string Fee, string Duration, DateTime StartDate)
        {
            List<InstituteCourse> InstituteCourses = db.InstituteCourse.ToList();
            foreach (var Courseids in CourseId)
            {
                foreach (var ci in InstituteCourses)
                {
                    if (ci.InstituteId == InstituteId && ci.CourseId == Courseids)
                    {
                        ViewBag.Error = "This relation is already exist.";
                        //ViewBag.InstituteId = new SelectList(db.institute, "InstituteId", "InstituteName");
                        var Course = db.Course.ToList();
                        return View(Course);
                    }
                }
            }
            foreach (var Courseids in CourseId)
            {
                InstituteCourse CourseInstitute = new InstituteCourse();
                CourseInstitute.InstituteId = InstituteId;
                CourseInstitute.CourseId = Courseids;
                CourseInstitute.Fee = Fee;
                CourseInstitute.Duration = Duration;
                CourseInstitute.StartDate = StartDate;
                db.InstituteCourse.Add(CourseInstitute);
                db.SaveChanges();
            }
            return RedirectToAction("index");
        }

        public ActionResult Edit(int? id, int? cid)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InstituteCourse InstituteCourses = db.InstituteCourse.Where(i => i.InstituteId == id && i.CourseId == cid).FirstOrDefault();
            if (InstituteCourses == null)
            {
                return HttpNotFound();
            }
            return View(InstituteCourses);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int CourseId, int InstituteId, string Fee, string Duration, DateTime StartDate)
        {
            InstituteCourse InstituteCourses = db.InstituteCourse.Where(i => i.InstituteId == InstituteId && i.CourseId == CourseId).FirstOrDefault();

            InstituteCourses.Fee = Fee;
            InstituteCourses.Duration = Duration;
            InstituteCourses.StartDate = StartDate;
            db.Entry(InstituteCourses).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        //GET: InstituteCourses/Delete/5
        public ActionResult Delete(int? id, int? cid)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InstituteCourse InstituteCourses = db.InstituteCourse.Where(i => i.InstituteId == id && i.CourseId == cid).FirstOrDefault();
            if (InstituteCourses == null)
            {
                return HttpNotFound();
            }
            return View(InstituteCourses);
        }

        // POST: InstituteCourses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, int cid)
        {
            InstituteCourse InstituteCourses = db.InstituteCourse.Where(i => i.InstituteId == id && i.CourseId == cid).FirstOrDefault();
            db.InstituteCourse.Remove(InstituteCourses);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
