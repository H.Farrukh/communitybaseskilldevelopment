﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using ProjectUni.Models;

//namespace ProjectUni.Controllers
//{
//    public class AuthorizeUserAccessLevel : AuthorizeAttribute
//    {
//        public string UserRole { get; set; }
//        protected override bool AuthorizeCore(HttpContextBase httpContext)
//        {
//             PContext db = new PContext();

//            var q = HttpContext.Current.Session["Role"];
//            var rid = HttpContext.Current.Session["UserID"].ToString();

//            if (q == null || rid == null)
//            {
//                return false;
//            }

//            var user = db.Registration.Find(rid);
//            if (user == null)
//            {
//                return false;
//            }


//            string w = q.ToString();

//            if (this.UserRole.Contains(w))
//            {
//                return true;
//            }
//            else
//            {
//                return false;
//            }
//        }
//    }
//}


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ProjectUni.Models;

namespace ProjectUni.Controllers
{
    public class AuthorizeUserAccessLevel : AuthorizeAttribute
    {
        public string UserRole { get; set; }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            PContext db = new PContext();

            var q = HttpContext.Current.Session["Role"];
            if (q == null)
            {
                return false;
            }
            var rid = HttpContext.Current.Session["Email"].ToString();
            
            if (q == null || rid == null)
            {
                return false;
            }

            string w = q.ToString();

            if (this.UserRole.Contains(w))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}