﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using ProjectUni.Models;

namespace ProjectUni.Controllers
{
    [AuthorizeUserAccessLevel(UserRole = "Candidate")]
    public class UsersController : Controller
    {
        private PContext db = new PContext();

        public ActionResult Jobs()
        {
            var jobs = db.Job.Where(j => j.ApplyBefore > DateTime.Now).ToList();
            return View(jobs);
        }

        public ActionResult PageDetail(int id)
        {

            var notes = db.Notes.Where(n => n.T_ID == id).FirstOrDefault();
            if (notes == null)
            {
                return RedirectToAction("Topics", "users", new { id = 0 });
            }

            return View(notes);
        }

        public ActionResult VideoDetail(int id)
        {

            var video = db.Videos.Where(n => n.T_ID == id).ToList();
            return PartialView("_VideoDetail", video);
        }

        public ActionResult Courses()
        {
            var InstituteCourses = db.InstituteCourse.Include(c => c.Course).Include(c => c.Institute).Where(i => i.StartDate > DateTime.Now);
            return View(InstituteCourses.ToList());
        }

        public ActionResult Coursedetail(int id)
        {
            var Institute = db.institute.Where(i => i.InstituteId == id).SingleOrDefault();
            return View(Institute);
        }

        public ActionResult Topics(int? id)
        {
            var topics = db.Topics.ToList();
            if(id == 0)
            {
                ViewBag.error = "Sorry: This topic does not contain content.";
            }
            return View(topics);
        }

        public ActionResult TestTopic(int? id)
        {
            if (id == 0)
            {
                ViewBag.TestAttempted = "You are not able to this test yet. You get next attempt after 3 months of test.";
            }
            if (id == -1)
            {
                ViewBag.error = "Sorry: This topic does not contain any content.";
            }
            var topics = db.Topics.ToList();
            return View(topics);
        }

        public ActionResult TestQuiz(int id)
        {
            string email = Session["Email"].ToString();
            var tdate = DateTime.Now.AddMonths(-3);
            var TestAttempted = db.Result.Where(rs => rs.Date > tdate && rs.UserEmail == email).ToList();
            if(TestAttempted.Any(t => t.T_ID == id))
            {
                return RedirectToAction("TestTopic",new { id = 0});
            }


            var Questions = db.Test.Where(q => q.T_ID == id).ToList();
            if (Questions == null || Questions.Count == 0)
            {
                return RedirectToAction("TestTopic", "users", new { id = -1 });
            }
            Random rnd = new Random();
            var counter = 3/*Questions.Count*/;
            int r = 0;
            List<Test> finalListb = new List<Test>();
            while (counter > 0)
            {
                r = rnd.Next(Questions.Count);
                var temp = Questions.ElementAt(r);
                counter--;
                if (!finalListb.Contains(temp))
                {
                    finalListb.Add(temp);
                }
            }
            
            return View(finalListb);
        }

        [HttpGet]
        public JsonResult Getques(int qid)
        {
            var ques = db.Test.Where(q => q.QuesId == qid).SingleOrDefault();
                return Json(ques, JsonRequestBehavior.AllowGet);
            
        }



        public ActionResult JobDetail(int id)
        {
            var applyby = Session["Email"].ToString();
            var jobapplication = db.JobApplication.Where(j => j.JobId == id && j.Applyby == applyby).SingleOrDefault();
            var jobs = db.Job.Where(j => j.JobId == id).FirstOrDefault();
            if (jobs == null)
            {
               return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (jobapplication != null)
            {
                ViewBag.status = 1;
                return View(jobs);
            }
            return View(jobs);
        }

        public ActionResult ApplyforJob(int id)
        {
                var applyjob = new JobApplication();
                applyjob.JobId = id;
                applyjob.Applyby = Session["Email"].ToString();
                db.JobApplication.Add(applyjob);
                db.SaveChanges();
                return RedirectToAction("Jobs");
        }


        [AuthorizeUserAccessLevel(UserRole = "Candidate")]
        // GET: Users/Details/5
        public ActionResult ViewInfo_P(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (id != (int)Session["CandidateID"])
            {
                return HttpNotFound();
            }
            Users users = db.users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            Users userPer = new Users();
            userPer.UserId = users.UserId;
            userPer.FirstName = users.FirstName;
            userPer.LastName = users.LastName;
            userPer.Email = users.Email;
            userPer.Gender = users.Gender;
            userPer.DateofBirth = users.DateofBirth;
            userPer.Contact = users.Contact;
            userPer.CNIC = users.CNIC;
            userPer.Address = users.Address;
            userPer.Image = users.Image;
            userPer.CityId = users.CityId;
            userPer.Cities = db.cities.Find(users.CityId);


            return View(userPer);
        }

        [AuthorizeUserAccessLevel(UserRole = "Candidate")]
        // GET: Users/Details/5
        public ActionResult ViewInfo_Ed(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (id != (int)Session["CandidateID"])
            {
                return HttpNotFound();
            }
            Users users = db.users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            Users userEdu = new Users();
            userEdu.UserId = users.UserId;
            userEdu.LevelId = users.LevelId;
            userEdu.DegreeId = users.DegreeId;
            userEdu.Institute = users.Institute;
            userEdu.DateofCompletion = users.DateofCompletion;
            userEdu.Grade = users.Grade;
            userEdu.Skills = users.Skills;
            userEdu.Image = users.Image;
            userEdu.degreearea = db.DegreeAreas.Find(users.DegreeId);
            userEdu.degreelevel = db.DegreeLevels.Find(users.LevelId);
            return View(userEdu);
        }

        [AuthorizeUserAccessLevel(UserRole = "Candidate")]
        // GET: Users/Details/5
        public ActionResult ViewInfo_Ex(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (id != (int)Session["CandidateID"])
            {
                return HttpNotFound();
            }
            Users users = db.users.Find(id);

            if (users == null)
            {
                return HttpNotFound();
            }
            Users userEx = new Users();
            userEx.UserId = users.UserId;
            userEx.OrganizationName = users.OrganizationName;
            userEx.Image = users.Image;
            userEx.Responsibility = users.Responsibility;
            userEx.Duration = users.Duration;

            return View(userEx);
        }

        [AuthorizeUserAccessLevel(UserRole = "Candidate")]
        // GET: Users/Create
        public ActionResult InsertInfo(int? id, string actid)

        {
            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName");
            ViewBag.DegreeId = new SelectList(db.DegreeAreas, "DegreeId", "AreaName");
            ViewBag.LevelId = new SelectList(db.DegreeLevels, "LevelId", "LevelName");
            if (id == null)
            {
                return View();
            }
            Users users = db.users.Find(id);
            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", users.CityId);
            ViewBag.DegreeId = new SelectList(db.DegreeAreas, "DegreeId", "AreaName");
            ViewBag.LevelId = new SelectList(db.DegreeLevels, "LevelId", "LevelName", users.LevelId);
            ViewBag.action = actid;
            var temp = db.DegreeAreas.Find(users.DegreeId);
            if (temp != null)
                ViewBag.deg = temp.AreaName;
            if (users == null)
            {
                return HttpNotFound();
            }

            return View(users);
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InsertInfo([Bind(Include = "FirstName,LastName,Email,Gender,Image,DateofBirth,Contact,CNIC,Address,CityId,LevelId,DegreeId,Institute,DateofCompletion,Grade,Skills,OrganizationName,Duration,Responsibility")] Users users, int? UserId, DateTime? DateofCompletion, HttpPostedFileBase Image)
        {
            if (ModelState.IsValid)
            {

                if (Image != null)
                {

                    var filename = Path.GetFileName(Image.FileName);
                    var extension = Path.GetExtension(filename).ToLower();
                    if (extension == ".jpg" || extension == ".png" || extension == ".jpeg" || extension == ".gif" || extension == ".bmp" || extension == ".jpeg" || extension == ".gif" || extension == ".bmp")
                    {
                        var path = HostingEnvironment.MapPath(Path.Combine("~/ProfileImages/", filename));
                        Image.SaveAs(path);
                        users.Image = "~/ProfileImages/" + filename;
                        if (UserId != null)
                        {
                            Users user = db.users.Find(UserId);
                            user.Image = "~/ProfileImages/" + filename;
                            db.Entry(user).State = EntityState.Modified;
                            db.SaveChanges();
                            return RedirectToAction("ViewInfo_p", "Users", new { id = user.UserId });
                        }
                        db.users.Add(users);
                        db.SaveChanges();
                        return RedirectToAction("ViewInfo_p", "Users", new { id = users.UserId, actid = "Ex" });

                    }
                    if (extension != ".jpg" || extension != ".png" || extension != ".jpeg" || extension != ".gif" || extension != ".bmp" || extension != ".jpeg" || extension != ".gif" || extension != ".bmp")
                    {
                        ViewBag.ImageError = "Choose the image file.";
                        ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", users.CityId);
                        ViewBag.DegreeId = new SelectList(db.DegreeAreas, "DegreeId", "AreaName", users.DegreeId);
                        ViewBag.LevelId = new SelectList(db.DegreeLevels, "LevelId", "LevelName", users.LevelId);
                        return View(users);
                    }
                }
                Users usere = db.users.Find(UserId);
                usere.LevelId = users.LevelId;
                usere.DegreeId = users.DegreeId;
                usere.Institute = users.Institute;
                usere.DateofCompletion = users.DateofCompletion;
                usere.Grade = users.Grade;
                usere.Skills = users.Skills;
                usere.OrganizationName = users.OrganizationName;
                usere.Duration = users.Duration;
                usere.Responsibility = users.Responsibility;
                db.Entry(usere).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ViewInfo_p", "Users", new { id = usere.UserId });
            }
            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", users.CityId);
            ViewBag.DegreeId = new SelectList(db.DegreeAreas, "DegreeId", "AreaName", users.DegreeId);
            ViewBag.LevelId = new SelectList(db.DegreeLevels, "LevelId", "LevelName", users.LevelId);
            return View(users);
        }


        public ActionResult getDegreeArea(int LevelId)
        {
            ViewBag.DegreeId = new SelectList(db.DegreeAreas.Where(d => d.LevelId == LevelId), "DegreeId", "AreaName", ViewBag.deg);
            return PartialView("DisplayDegree");
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            ViewBag.DegreeId = new SelectList(db.DegreeAreas, "DegreeId", "AreaName", users.DegreeId);
            ViewBag.LevelId = new SelectList(db.DegreeLevels, "LevelId", "LevelName");
            return View(users);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserId,FirstName,LastName,Email,Gender,Image,DateofBirth,Contact,CNIC,Address,Password,ConfirmPassword,LevelId,DegreeId,Institute,DateofCompletion,Grade,Skills,OrganizationName,Duration,Responsibility")] Users users)
        {
            if (ModelState.IsValid)
            {
                db.Entry(users).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DegreeId = new SelectList(db.DegreeAreas, "DegreeId", "AreaName", users.DegreeId);
            ViewBag.LevelId = new SelectList(db.DegreeLevels, "LevelId", "LevelName");
            return View(users);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Users users = db.users.Find(id);
            db.users.Remove(users);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
