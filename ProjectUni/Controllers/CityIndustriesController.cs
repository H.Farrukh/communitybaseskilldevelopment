﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectUni.Models;

namespace ProjectUni.Controllers
{
    public class CityIndustriesController : Controller
    {
        private PContext db = new PContext();

        // GET: CityIndustries
        public ActionResult Index()
        {
            var cityIndustries = db.cityIndustries.Include(c => c.cities).Include(c => c.industries);
            return View(cityIndustries.ToList());
        }

        // GET: cityindustriess/Create
        public ActionResult CityAssignIndustries()
        {
            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName");
            var industry = db.industries.ToList();
            return View(industry);
        }

        // POST: cityindustriess/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CityAssignIndustries(int CityId, int[] IndustryId)
        {
            List<CityIndustries> cityindustries = db.cityIndustries.ToList();
            
            foreach (var industryids in IndustryId)
            {
                
                foreach (var ci in cityindustries)
                {
                    


                    if (ci.CityId == CityId && ci.IndustryId == industryids)
                    {
                        ViewBag.Error = "This relation is already exist.";
                        ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName");
                        var industry = db.industries.ToList();
                        return View(industry);
                    }

                }

                

            }
            foreach (var industryids in IndustryId)
            {
                CityIndustries cityIndustry = new CityIndustries();
                cityIndustry.CityId = CityId;
                cityIndustry.IndustryId = industryids;
                db.cityIndustries.Add(cityIndustry);
                db.SaveChanges();
            }
            return RedirectToAction("index");
        }

        // GET: CityIndustries/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CityIndustries cityIndustries = db.cityIndustries.Find(id);
            if (cityIndustries == null)
            {
                return HttpNotFound();
            }
            return View(cityIndustries);
        }

        // POST: CityIndustries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CityIndustries cityIndustries = db.cityIndustries.Find(id);
            db.cityIndustries.Remove(cityIndustries);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
