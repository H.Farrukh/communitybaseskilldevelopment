﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectUni.Models;
using PagedList;
using PagedList.Mvc;


namespace ProjectUni.Controllers
{
    [AuthorizeUserAccessLevel(UserRole = "Admin")]
    public class NotesController : Controller
    {
        private PContext db = new PContext();

        // GET: Notes
        public ActionResult Index(int? page, string search)
        {
            var notes = db.Notes.Include(n => n.topics);
            notes = db.Notes.AsQueryable();
            notes = db.Notes.Where(x => x.topics.TName.Contains(search) || search == null);
            return View(notes.ToList().OrderByDescending(x=>x.N_ID).ToPagedList(page ?? 1, 4));
        }

        

        // GET: Notes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Notes notes = db.Notes.Find(id);
            if (notes == null)
            {
                return HttpNotFound();
            }
            return View(notes);
        }

        // GET: Notes/Create
        public ActionResult Create()
        {
            ViewBag.T_ID = new SelectList(db.Topics, "T_ID", "TName");
            return View();
        }

        // POST: Notes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "N_ID,TNotes,T_ID")] Notes notes)
        {
            if (ModelState.IsValid)
            {
                db.Notes.Add(notes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.T_ID = new SelectList(db.Topics, "T_ID", "TName", notes.T_ID);
            return View(notes);
        }

        // GET: Notes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Notes notes = db.Notes.Find(id);
            if (notes == null)
            {
                return HttpNotFound();
            }
            ViewBag.T_ID = new SelectList(db.Topics, "T_ID", "TName", notes.T_ID);
            return View(notes);
        }

        // POST: Notes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "N_ID,TNotes,T_ID")] Notes notes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(notes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.T_ID = new SelectList(db.Topics, "T_ID", "TName", notes.T_ID);
            return View(notes);
        }

        // GET: Notes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Notes notes = db.Notes.Find(id);
            if (notes == null)
            {
                return HttpNotFound();
            }
            return View(notes);
        }

        // POST: Notes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Notes notes = db.Notes.Find(id);
            db.Notes.Remove(notes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
