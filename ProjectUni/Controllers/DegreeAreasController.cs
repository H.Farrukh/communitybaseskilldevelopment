﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectUni.Models;

namespace ProjectUni.Controllers
{
    [AuthorizeUserAccessLevel(UserRole = "Admin")]
    public class DegreeAreasController : Controller
    {
        private PContext db = new PContext();

        // GET: DegreeAreas
        public ActionResult Index()
        {
            var degreeAreas = db.DegreeAreas.Include(d => d.degreelevel);
            return View(degreeAreas.ToList());
        }

        // GET: DegreeAreas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DegreeArea degreeArea = db.DegreeAreas.Find(id);
            if (degreeArea == null)
            {
                return HttpNotFound();
            }
            return View(degreeArea);
        }

        // GET: DegreeAreas/Create
        public ActionResult Create()
        {
            ViewBag.LevelId = new SelectList(db.DegreeLevels, "LevelId", "LevelName");
            return View();
        }

        // POST: DegreeAreas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DegreeId,AreaName,LevelId")] DegreeArea degreeArea)
        {
            if (ModelState.IsValid)
            {
                db.DegreeAreas.Add(degreeArea);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LevelId = new SelectList(db.DegreeLevels, "LevelId", "LevelName", degreeArea.LevelId);
            return View(degreeArea);
        }

        // GET: DegreeAreas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DegreeArea degreeArea = db.DegreeAreas.Find(id);
            if (degreeArea == null)
            {
                return HttpNotFound();
            }
            ViewBag.LevelId = new SelectList(db.DegreeLevels, "LevelId", "LevelName", degreeArea.LevelId);
            return View(degreeArea);
        }

        // POST: DegreeAreas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DegreeId,AreaName,LevelId")] DegreeArea degreeArea)
        {
            if (ModelState.IsValid)
            {
                db.Entry(degreeArea).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LevelId = new SelectList(db.DegreeLevels, "LevelId", "LevelName", degreeArea.LevelId);
            return View(degreeArea);
        }

        // GET: DegreeAreas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DegreeArea degreeArea = db.DegreeAreas.Find(id);
            if (degreeArea == null)
            {
                return HttpNotFound();
            }
            return View(degreeArea);
        }

        // POST: DegreeAreas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DegreeArea degreeArea = db.DegreeAreas.Find(id);
            db.DegreeAreas.Remove(degreeArea);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
