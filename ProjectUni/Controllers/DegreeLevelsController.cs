﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectUni.Models;

namespace ProjectUni.Controllers
{
    [AuthorizeUserAccessLevel(UserRole = "Admin")]
    public class DegreeLevelsController : Controller
    {
        private PContext db = new PContext();

        // GET: DegreeLevels
        public ActionResult Index()
        {
            return View(db.DegreeLevels.ToList());
        }

        // GET: DegreeLevels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DegreeLevel degreeLevel = db.DegreeLevels.Find(id);
            if (degreeLevel == null)
            {
                return HttpNotFound();
            }
            return View(degreeLevel);
        }

        // GET: DegreeLevels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DegreeLevels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LevelId,LevelName")] DegreeLevel degreeLevel)
        {
            if (ModelState.IsValid)
            {
                db.DegreeLevels.Add(degreeLevel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(degreeLevel);
        }

        // GET: DegreeLevels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DegreeLevel degreeLevel = db.DegreeLevels.Find(id);
            if (degreeLevel == null)
            {
                return HttpNotFound();
            }
            return View(degreeLevel);
        }

        // POST: DegreeLevels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LevelId,LevelName")] DegreeLevel degreeLevel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(degreeLevel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(degreeLevel);
        }

        // GET: DegreeLevels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DegreeLevel degreeLevel = db.DegreeLevels.Find(id);
            if (degreeLevel == null)
            {
                return HttpNotFound();
            }
            return View(degreeLevel);
        }

        // POST: DegreeLevels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DegreeLevel degreeLevel = db.DegreeLevels.Find(id);
            db.DegreeLevels.Remove(degreeLevel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
