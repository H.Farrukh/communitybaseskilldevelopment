﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectUni.Models;

namespace ProjectUni.Controllers
{
    public class JobsController : Controller
    {
        private PContext db = new PContext();

        // GET: Jobs
        public ActionResult Index()
        {
            var job = db.Job.Include(j => j.Cities).Include(j => j.degreearea).Include(j => j.degreelevel).Include(j => j.Industries);
            return View(job.ToList());
        }

        public ActionResult Applyby(int id)
        {
            var job = db.JobApplication.Where(j => j.JobId == id).ToList();
            return View(job);
        }

        public ActionResult ApplybyDetail(int id)
        {
            var user = db.users.Where(j => j.UserId == id).FirstOrDefault();
            return View(user);
        }

        // GET: Jobs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job job = db.Job.Find(id);
            if (job == null)
            {
                return HttpNotFound();
            }
            return View(job);
        }

        // GET: Jobs/Create
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName");
            ViewBag.DegreeId = new SelectList(db.DegreeAreas, "DegreeId", "AreaName");
            ViewBag.LevelId = new SelectList(db.DegreeLevels, "LevelId", "LevelName");
            ViewBag.IndustryId = new SelectList(db.industries, "IndustryId", "IndustryName");
            ViewBag.JobDesignation = new SelectList(db.fields, "FieldId", "FieldName");
            var ob = db.Registration.Find(Session["UserID"]);
            var cm = db.Companies.Where(c => c.Email == ob.Email).SingleOrDefault();
            ViewBag.CompanyId = cm.CompanyName;
            return View();
        }

        // POST: Jobs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "JobId,JobTitle,JobDesignation,JobDescription,Skills,JobType,CityId,JobShift,IndustryId,CareerLevel,Experience,MinSalary,MixSalary,Gender,Positions,JobDate,ApplyBefore,MinAge,MaxAge,LevelId,DegreeId,Status,Company")] Job job)
        {
            if (ModelState.IsValid)
            {
                job.JobDate = DateTime.Now.ToString();
                db.Job.Add(job);
                db.SaveChanges();
                return RedirectToAction("JobIndex","Companies");
            }

            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", job.CityId);
            ViewBag.DegreeId = new SelectList(db.DegreeAreas, "DegreeId", "AreaName", job.DegreeId);
            ViewBag.LevelId = new SelectList(db.DegreeLevels, "LevelId", "LevelName", job.LevelId);
            ViewBag.IndustryId = new SelectList(db.industries, "IndustryId", "IndustryName", job.IndustryId);
            ViewBag.JobDesignation = new SelectList(db.fields, "FieldId", "FieldName");
            return View(job);
        }

        // GET: Jobs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job job = db.Job.Find(id);
            if (job == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", job.CityId);
            ViewBag.DegreeId = new SelectList(db.DegreeAreas, "DegreeId", "AreaName", job.DegreeId);
            ViewBag.LevelId = new SelectList(db.DegreeLevels, "LevelId", "LevelName", job.LevelId);
            ViewBag.IndustryId = new SelectList(db.industries, "IndustryId", "IndustryName", job.IndustryId);
            return View(job);
        }

        // POST: Jobs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "JobId,JobTitle,JobDesignation,JobDescription,Skills,JobType,CityId,JobShift,IndustryId,CareerLevel,Experience,MinSalary,MixSalary,Gender,Positions,JobDate,ApplyBefore,MinAge,MaxAge,LevelId,DegreeId,Status,Company")] Job job)
        {
            if (ModelState.IsValid)
            {
                db.Entry(job).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("JobIndex", "companies");
            }
            ViewBag.CityId = new SelectList(db.cities, "CityId", "CityName", job.CityId);
            ViewBag.DegreeId = new SelectList(db.DegreeAreas, "DegreeId", "AreaName", job.DegreeId);
            ViewBag.LevelId = new SelectList(db.DegreeLevels, "LevelId", "LevelName", job.LevelId);
            ViewBag.IndustryId = new SelectList(db.industries, "IndustryId", "IndustryName", job.IndustryId);
            return View(job);
        }

        // GET: Jobs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job job = db.Job.Find(id);
            if (job == null)
            {
                return HttpNotFound();
            }
            return View(job);
        }

        // POST: Jobs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Job job = db.Job.Find(id);
            db.Job.Remove(job);
            db.SaveChanges();
            return RedirectToAction("JobIndex","companies");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
