﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectUni.Models;
using PagedList;
using PagedList.Mvc;

namespace ProjectUni.Controllers
{
    [AuthorizeUserAccessLevel(UserRole = "Admin")]
    public class VideosController : Controller
    {
        private PContext db = new PContext();

        // GET: Videos
        public ActionResult Index(int? page, string search)
        {
            var videos = db.Videos.Include(v => v.topics);
            videos = db.Videos.AsQueryable();
            videos = videos.Where(x => x.topics.TName.Contains(search) || search == null);
            return View(videos.ToList().OrderByDescending(x=>x.V_ID).ToPagedList(page ?? 1, 4));
        }

        // GET: Videos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Videos videos = db.Videos.Find(id);
            if (videos == null)
            {
                return HttpNotFound();
            }
            return View(videos);
        }

        // GET: Videos/Create
        public ActionResult Create()
        {
            ViewBag.T_ID = new SelectList(db.Topics, "T_ID", "TName");
            return View();
        }

        // POST: Videos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "V_ID,Name,URL,T_ID")] Videos videos)
        {
            if (ModelState.IsValid)
            {
                videos.URL = videos.URL.Replace("watch?v=", "embed/");
                db.Videos.Add(videos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.T_ID = new SelectList(db.Topics, "T_ID", "TName", videos.T_ID);
            return View(videos);
        }

        // GET: Videos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Videos videos = db.Videos.Find(id);
            if (videos == null)
            {
                return HttpNotFound();
            }
            ViewBag.T_ID = new SelectList(db.Topics, "T_ID", "TName", videos.T_ID);
            return View(videos);
        }

        // POST: Videos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "V_ID,Name,URL,T_ID")] Videos videos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(videos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.T_ID = new SelectList(db.Topics, "T_ID", "TName", videos.T_ID);
            return View(videos);
        }

        // GET: Videos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Videos videos = db.Videos.Find(id);
            if (videos == null)
            {
                return HttpNotFound();
            }
            return View(videos);
        }

        // POST: Videos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Videos videos = db.Videos.Find(id);
            db.Videos.Remove(videos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
