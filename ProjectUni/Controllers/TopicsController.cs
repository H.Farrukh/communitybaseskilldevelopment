﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectUni.Models;

namespace ProjectUni.Controllers
{
    [AuthorizeUserAccessLevel(UserRole = "Admin")]
    public class TopicsController : Controller
    {
        private PContext db = new PContext();

        // GET: Topics
        public ActionResult Index()
        {
            var topics = db.Topics.Include(t => t.Fields);
            return View(topics.ToList());
        }

        // GET: Topics
        public ActionResult PageDetail()
        {
            var notes = db.Notes.Include(t => t.topics);
            return View(notes.ToList());
        }

        // GET: Topics
        public ActionResult VideoDetail()
        {
            var video = db.Videos.Include(t => t.topics);
            return View(video.ToList());
        }

        // GET: Topics/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Topics topics = db.Topics.Find(id);
            if (topics == null)
            {
                return HttpNotFound();
            }
            return View(topics);
        }

        // GET: Topics/Create
        public ActionResult Create()
        {
            ViewBag.FieldId = new SelectList(db.fields, "FieldId", "FieldName");
            return View();
        }

        // POST: Topics/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "T_ID,TName,FieldId")] Topics topics)
        {
            var temp = db.Topics.Where(t => t.TName == topics.TName).SingleOrDefault();
            if (temp != null)
            {
                ViewBag.error = "Topic already exists.";
                ViewBag.FieldId = new SelectList(db.fields, "FieldId", "FieldName", topics.FieldId);
                return View(topics);
            }
            if (ModelState.IsValid)
            {
                db.Topics.Add(topics);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FieldId = new SelectList(db.fields, "FieldId", "FieldName", topics.FieldId);
            return View(topics);
        }

        // GET: Topics/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Topics topics = db.Topics.Find(id);
            if (topics == null)
            {
                return HttpNotFound();
            }
            ViewBag.FieldId = new SelectList(db.fields, "FieldId", "FieldName", topics.FieldId);
            return View(topics);
        }

        // POST: Topics/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "T_ID,TName,FieldId")] Topics topics)
        {
            if (ModelState.IsValid)
            {
                db.Entry(topics).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FieldId = new SelectList(db.fields, "FieldId", "FieldName", topics.FieldId);
            return View(topics);
        }

        // GET: Topics/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Topics topics = db.Topics.Find(id);
            if (topics == null)
            {
                return HttpNotFound();
            }
            return View(topics);
        }

        // POST: Topics/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Topics topics = db.Topics.Find(id);
            db.Topics.Remove(topics);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
