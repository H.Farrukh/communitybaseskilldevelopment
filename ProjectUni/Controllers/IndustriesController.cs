﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectUni.Models;

namespace ProjectUni.Controllers
{
    [AuthorizeUserAccessLevel(UserRole = "Admin")]
    public class IndustriesController : Controller
    {
        private PContext db = new PContext();

        // GET: Industries
        public ActionResult Index()
        {
            return View(db.industries.ToList());
        }

        // GET: Industries/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Industries industries = db.industries.Find(id);
            if (industries == null)
            {
                return HttpNotFound();
            }
            return View(industries);
        }

        // GET: Industries/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Industries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IndustryId,IndustryName")] Industries industries)
        {
            if (ModelState.IsValid)
            {
                db.industries.Add(industries);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(industries);
        }

        // GET: Industries/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Industries industries = db.industries.Find(id);
            if (industries == null)
            {
                return HttpNotFound();
            }
            return View(industries);
        }

        // POST: Industries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IndustryId,IndustryName")] Industries industries)
        {
            if (ModelState.IsValid)
            {
                db.Entry(industries).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(industries);
        }

        // GET: Industries/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Industries industries = db.industries.Find(id);
            if (industries == null)
            {
                return HttpNotFound();
            }
            return View(industries);
        }

        // POST: Industries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Industries industries = db.industries.Find(id);
            db.industries.Remove(industries);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
